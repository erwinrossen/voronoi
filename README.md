# Voronoi Tessellations

This code generates a Voronoi-Poisson tessellation, meaning it generates a Poisson point process and then uses it to generate a corresponding _Voronoi tesselation_: a partition of a plane into regions close to each of a given set of objects.

The code is based on [this blog post](https://hpaulkeeler.com/voronoi-dirichlet-tessellations/) from Paul Keeler.

![Voronoi tessellation](example/tessellation.png)

In order to run this code, you need to have Python 3 installed, and the requirements in `requirements/local.txt`. You can then run the code by running `python src/voronoi.py` in the terminal. The code will display the output in Python's default image viewer. If you want to save an image, add `plt.savefig('tessellation.png')` to the end of the file `voronoi.py`.
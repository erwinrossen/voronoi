# This code generates a Voronoi-Poisson tessellation, meaning it generates
# a Poisson point process and then uses it to generate a corresponding 
# Voronoi tesselation. A Voronoi tesselation is also known as a Dirichlet
# tesselation or Voronoi diagram.
#
# A (homogeneous) Poisson point process (PPP) is created on a rectangle.
# Then the Voronoi tesselation is found using the SciPy function 
# Voronoi[1], which is based on the Qhull project[2] .
#
# All points and Voronoi cells of the PPP are numbered arbitrarily.
#
# Author: H.Paul Keeler, 2019
# Website: hpaulkeeler.com
# Repository: github.com/hpaulkeeler/posts
# For more details, see the post:
# https://hpaulkeeler.com/voronoi-dirichlet-tessellations/
#
# [1] http://scipy.github.io/devdocs/generated/scipy.spatial.Voronoi.html
# [2] http://www.qhull.org/

import numpy as np  # NumPy package for arrays, random number generation, etc
import matplotlib.pyplot as plt  # for plotting
from scipy.spatial import Voronoi, voronoi_plot_2d  # for voronoi tessellation

# Simulation window parameters
x_min = 0
x_max = 1
y_min = 0
y_max = 1
# Rectangle dimensions
x_delta = x_max - x_min  # width
y_delta = y_max - y_min  # height
area_total = x_delta * y_delta  # area of similation window
# Point process parameters
lambda_0 = 10  # intensity (ie mean density) of the Poisson process


def get_poisson_points() -> np.ndarray:
    """
    Return a list of coordinates inside the simulation window generated using the Poisson distribution

    :return [[0.94552086 0.45992294]
             [0.53913136 0.95474073]
             [0.98099035 0.28851996]
             [0.44551371 0.40549633]] <class 'numpy.ndarray'>
    """

    nr_points = np.random.poisson(lambda_0 * area_total)  # Poisson number of points
    xx = x_delta * np.random.uniform(0, 1, nr_points) + x_min  # x coordinates of Poisson points
    yy = y_delta * np.random.uniform(0, 1, nr_points) + y_min  # y coordinates of Poisson points
    return np.stack((xx, yy), axis=1)  # combine x and y coordinates


def voronoi_tessellation(xxyy: np.ndarray) -> Voronoi:
    return Voronoi(xxyy)


def plot_diagram(xxyy: np.ndarray, voronoi_data: Voronoi):
    # Create voronoi diagram on the point pattern
    voronoi_plot_2d(voronoi_data, show_points=False, show_vertices=False)
    # Plot underlying point pattern (ie a realization of a Poisson point process)
    xx, yy = np.transpose(xxyy)
    plt.scatter(xx, yy, edgecolor='b', facecolor='b')
    # Number the points
    for i in range(len(xxyy)):
        plt.text(xx[i] + x_delta / 50, yy[i] + y_delta / 50, i + 1)


if __name__ == '__main__':
    plt.close('all')
    points = get_poisson_points()
    voronoi = voronoi_tessellation(points)
    plot_diagram(points, voronoi)
    plt.show()
